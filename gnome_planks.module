<?php

require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'gnome_planks') . '/gnome_planks.box.inc';

/**
 * Implements hook_permission().
 */
function gnome_planks_permission() {
  return array(
    'administer gnome_planks' => array(
      'title' => t('Administer Gnome Planks'),
      'restrict access' => TRUE
    ),
    'administer gnome_planks items' => array(
      'title' => t('Administer items in Gnome Planks Boxes'),
      'restrict access' => FALSE
    )
  );
}

/**
 * Implements hook_theme().
 */
function gnome_planks_theme() {
  return array(
    'gnome_planks_handlers_admin' => array(
      'render element' => 'elements',
    ),
    'gnome_planks_box_context_select_form_table' => array(
      'render element' => 'elements',
      'file' => 'gnome_planks.admin.inc',
    ),
    'gnome_planks_admin_box_items_form_table' => array(
      'render element' => 'elements',
      'file' => 'gnome_planks.admin.inc',
    ),
  );
}

/**
 * Implements hook_ctools_plugin_type().
 */
function gnome_planks_ctools_plugin_type() {
  return array(
    'context_sources' => array(),
    'context_handlers' => array(),
  );
}

/**
 * Implements hook_menu().
 */
function gnome_planks_menu() {
  $items = array();

  $items['admin/content/gnome_planks'] = array(
    'title' => 'Gnome Planks',
    'description' => 'Manages the content of Gnome Planks boxes.',
    'page callback' => 'gnome_planks_admin_page',
    'access arguments' => array('administer gnome_planks items'),
    'type' => MENU_LOCAL_TASK | MENU_NORMAL_ITEM,
    'file' => 'gnome_planks.admin.inc',
  );

  $items['admin/content/gnome_planks/%gnome_planks_box'] = array(
    'title callback' => 'gnome_planks_box_title',
    'title arguments' => array(3),
    'page callback' => 'gnome_planks_admin_box_page',
    'page arguments' => array(3),
    'access arguments' => array('administer gnome_planks items'),
    'type' => MENU_CALLBACK,
    'file' => 'gnome_planks.admin.inc'
  );

  $items['admin/content/gnome_planks/%gnome_planks_box/%'] = array(
    'title callback' => 'gnome_planks_box_title',
    'title arguments' => array(3),
    'page callback' => 'gnome_planks_admin_box_items_page',
    'page arguments' => array(3, 4),
    'access arguments' => array('administer gnome_planks items'),
    'type' => MENU_CALLBACK,
    'file' => 'gnome_planks.admin.inc'
  );
  $items['admin/content/gnome_planks/%gnome_planks_box/%/add'] = array(
    'title' => 'Add',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('gnome_planks_admin_box_item_add_form', 3, 4),
    'access arguments' => array('administer gnome_planks items'),
    'type' => MENU_LOCAL_ACTION,
    'file' => 'gnome_planks.admin.inc'
  );
  $items['admin/content/gnome_planks/%gnome_planks_box/%/%/delete'] = array(
    'title' => 'Delete',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('gnome_planks_admin_box_item_delete_form', 3, 4, 5),
    'access arguments' => array('administer gnome_planks items'),
    'type' => MENU_CALLBACK,
    'file' => 'gnome_planks.admin.inc'
  );

  return $items;
}

/**
 * Implements hook_block_info().
 */
function gnome_planks_block_info() {
  $blocks = array();

  ctools_include('export');
  $boxes = ctools_export_load_object('gnome_planks_boxes', 'all');

  foreach ($boxes as $box) {
    $blocks[$box->name] = array(
      'info' => t('Gnome Planks Box: !box', array('!box' => $box->label)),
      'cache' => DRUPAL_NO_CACHE, // maybe we should come up with something
        // smarter then this. The problem is that the caching should depend on
        // how the context is constructed, which can get very complex. A few
        // scenarios:
        //  - No context handlers on any source: DRUPAL_CACHE_GLOBAL
        //  - Just a set of handlers on path: DRUPAL_CACHE_PER_PAGE
        //  - A handler on path and a role handler on user: DRUPAL_CACHE_PER_PAGE | DRUPAL_CACHE_PER_ROLE
        //  - A handler on path and a (non-role) handler on user: DRUPAL_CACHE_PER_PAGE | DRUPAL_CACHE_PER_USER
    );
  }

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function gnome_planks_block_view($delta = '') {
  // load the box
  $box = gnome_planks_box_load($delta);
  if (!$box) {
    return;
  }

  // get a hash for the current context. returning FALSE is not an error. It
  // just means that there is no context, which is a perfectly valid thing
  $context_hash = gnome_planks_get_current_context_hash($box);
  if (!$context_hash) {
    return;
  }

  $admin = user_access('administer gnome_planks items'); // need something better then this

  // if the user is allowed to manage this box, we should make sure that we have
  // this context registered in the database
  if ($admin) {
    $context = gnome_planks_get_current_context($box);
    gnome_planks_ensure_context($context);
  }

  // get the items
  $items = gnome_planks_get_items($box, $context_hash);

  // if no items and not rights to manage, we display nothing
  if (!$items && !$admin) {
    return;
  }

  // get the render arrays for the items
  $views = gnome_planks_view_items($box, $items);

  // create our render array
  $result = array('items' => array());
  foreach ($items as $item) {
    $result['items'][$item->item_id] = $views[$item->entity_type][$item->entity_id];
  }

  // add admin link
  if ($admin) {
    $result['admin'] = array(
      '#theme' => 'link',
      '#path' => 'admin/content/gnome_planks/' . $box->name . '/' . $context_hash,
      '#text' => t('Manage items'),
      '#options' => array(
        'attributes' => array(
          'class' => array(
            'gnome-planks-admin-link'
          )
        ),
        'html' => FALSE
      ),
    );
  }

  return array(
    'content' => $result
  );
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function gnome_planks_ctools_plugin_directory($module, $plugin) {
  $types = gnome_planks_ctools_plugin_type();
  if ($module == 'gnome_planks' && isset($types[$plugin])) {
    return 'plugins/' . $plugin;
  }
  if ($module == 'ctools' && $plugin == 'export_ui') {
    return 'plugins/export_ui';
  }
}

/**
 * Generate some unique ID. This is used in the UI to assign ID's to the
 * handlers without a link to their weight.
 */
function _gnome_planks_id() {
  static $id = 9876543210;
  return md5(strval(time()) . strval($id++));
}

/**
 * Returns a box object.
 */
function gnome_planks_box_load($name) {
  ctools_include('export');
  $result = ctools_export_load_object('gnome_planks_boxes', 'names', array($name));
  if (isset($result[$name])) {
    return $result[$name];
  }
}
/**
 * Menu title callback.
 */
function gnome_planks_box_title($box) {
  return $box->label;
}

/**
 * Get CTools plugin data for context_sources.
 */
function gnome_planks_get_context_sources($context_source = NULL) {
  // we leave all caching to CTools
  ctools_include('plugins');
  $result = ctools_get_plugins('gnome_planks', 'context_sources', $context_source);
  if ($context_source) {
    ksort($result);
  }
  return $result;
}

/**
 * Get CTools plugin data for context_handlers.
 */
function gnome_planks_get_context_handlers($context_handlers = NULL) {
  // we leave all caching to CTools
  ctools_include('plugins');
  return ctools_get_plugins('gnome_planks', 'context_handlers', $context_handlers);
}

/**
 * Get Context Source object. Returns an initialized context_source.
 */
function gnome_planks_get_source($context_source) {
  $cache = &drupal_static(__FUNCTION__, array());

  // create new object
  if (!isset($cache[$context_source['name']])) {
    ctools_include('plugins');
    $class = ctools_plugin_get_class($context_source, 'source class');

    if ($class) {
      $cache[$context_source['name']] = new $class;
    }
    else{
      $cache[$context_source['name']] = FALSE;
    }
  }

  return $cache[$context_source['name']];
}

/**
 * Get Context Handler object. Returns an initialized context_handler.
 */
function gnome_planks_get_handler($context_handler, $options) {
  $cache = &drupal_static(__FUNCTION__, array());

  // this hash is not for security purposes, but just to compare options arrays
  // and to prevent extremely lengthy array keys
  $options_hash = md5(serialize($options));

  if (!isset($cache[$context_handler['name']][$options_hash])) {
    ctools_include('plugins');
    $class = ctools_plugin_get_class($context_handler, 'handler class');

    if ($class) {
      $cache[$context_handler['name']][$options_hash] = new $class($options);
    }
    else{
      $cache[$context_handler['name']][$options_hash] = FALSE;
    }
  }

  return $cache[$context_handler['name']][$options_hash];
}

/**
 * Get the items from the database for a box and a context
 */
function gnome_planks_get_items($box, $context_hash) {
  $name = is_object($box) ? $box->name : $box;

  $items = db_select('gnome_planks_items', 'i')
    ->fields('i', array('item_id', 'box', 'context', 'weight', 'entity_type', 'entity_id'))
    ->condition('box', $name)->condition('context', $context_hash)
    ->orderBy('weight')
    ->orderBy('item_id') // add sorting on item_id to be sure that the items always come out in the same order
    ->execute()->fetchAllAssoc('item_id');

  return $items;
}

/**
 * Pass entities through entity_view().
 */
function gnome_planks_view_items($box, $items) {
  $entity_info = gnome_planks_get_entity_info();

  $entity_data = array();
  $entity_ids = array();
  foreach ($items as $item_id => $item) {
    $entity_ids[$item->entity_type][] = $item->entity_id;
  }

  $entities = array();
  foreach ($entity_ids as $entity_type => $ids) {
    $entities[$entity_type] = entity_load($entity_type, $ids);
  }

  $views = array();
  foreach ($entities as $entity_type => $_entities) {
    $views[$entity_type] = array();
    if (!empty($_entities)) {
      foreach ($_entities as $entity_id => $entity) {
        $bundle = $entity->{$entity_info[$entity_type]['entity keys']['bundle']};
        $views[$entity_type][$entity_id] = entity_view($entity_type, array($entity_id => $entity), $box->entity_types[$entity_type][$bundle]);
      }
    }
  }

  return $views;
}

/**
 * Convert context array to hash
 */
function gnome_planks_get_context_hash($context) {
  // !empty would not be good, because for boxes without handlers an empty
  // array is a perfectly valid context
  if (is_array($context)) {
    return md5(serialize($context));
  }
  else{
    return FALSE;
  }
}

/**
 * Get current context hash for a box.
 */
function gnome_planks_get_current_context_hash($box) {
  $context = gnome_planks_get_current_context($box);
  return gnome_planks_get_context_hash($context);
}

/**
 * Get current context for a box. We cache the result static
 */
function gnome_planks_get_current_context($box) {
  $cache = &drupal_static(__FUNCTION__, array());

  if (!isset($cache[$box->name])) {
    $cache[$box->name] = gnome_planks_build_current_context($box);
  }

  return $cache[$box->name];
}

/**
 * Build the current context for a box. We query all sources and pass the result
 * through the handlers.
 */
function gnome_planks_build_current_context($box) {
  $context_sources = gnome_planks_get_context_sources();
  $context_handlers = gnome_planks_get_context_handlers();

  $context = array();
  foreach ($context_sources as $name => $source_plugin) {
    // only handle the enabled context sources
    if (!isset($box->context_handlers[$name])) {
      continue;
    }

    // get the source class. Without a source class, the source is considered to
    // have no value and therefore there is no context
    $source = gnome_planks_get_source($source_plugin);
    if (!$source) {
      return FALSE;
    }

    // get the value from the source class
    $value = $source->get_value();
    if (!isset($value)) {
      return FALSE;
    }

    $result = NULL;

    foreach ($box->context_handlers[$name] as $handler_info) {
      // if the handler is not specified, we act like the handler did not
      // return data
      if (!isset($context_handlers[$handler_info->handler])) {
        continue;
      }

      // get the handler class. Without a handler class, we act like the
      // handler did not return data
      $handler = gnome_planks_get_handler($context_handlers[$handler_info->handler], $handler_info->options);
      if (!$handler) {
        continue;
      }

      $handler_result = $handler->handle_value($value);
      if (isset($handler_result)) {
        $result = array($handler_info->handler, $handler_result);
        break;
      }
    }

    if (isset($result)) {
      $context[$name] = $result;
    }
    else{
      return FALSE;
    }
  }

  return $context;
}

/**
 * Make sure a context exists in the database. We need this for admins. We want
 * a context to exist in the database before we create a link to it
 */
function gnome_planks_ensure_context($context) {
  // get the hash, because the context information is stored under the hash
  $hash = gnome_planks_get_context_hash($context);

  if (!$hash) {
    return FALSE;
  }

  // check if the context is already registered
  $existing = db_select('gnome_planks_contexts')->fields('gnome_planks_contexts')->condition('hash', $hash)->execute()->fetchAll();
  if ($existing) {
    return $hash;
  }

  // add the main record to the database
  db_insert('gnome_planks_contexts')->fields(array('hash'))->values(array('hash' => $hash))->execute();

  // !empty works fine here, because we know we have a context and the if here
  // is just to make sure that we are not going to try to insert an empty set.
  if (!empty($context)) {
    $query = db_insert('gnome_planks_context_values')->fields(array('context', 'source', 'value'));
    foreach ($context as $source => $value) {
      $query->values(array('context' => $hash, 'source' => $source, 'value' => serialize($value)));
    }
    $query->execute();
  }

  return $hash;
}

/**
 * Wrapper around entity_get_info that checks whether we have a load function
 * available.
 */
function gnome_planks_get_entity_info() {
  $entity_info = &drupal_static(__FUNCTION__, array());

  if (empty($entity_info)) {
    $base_entity_info = entity_get_info();

    foreach ($base_entity_info as $entity_type => $entity_data) {
      if (($modules = module_implements('gnome_planks_load_' . $entity_type)) && count($modules) == 1) {
        $entity_data['gnome_planks_module'] = $modules[0];
        $entity_info[$entity_type] = $entity_data;
      }
    }
  }

  return $entity_info;
}

/**
 * Entity load callback for nodes
 */
function gnome_planks_gnome_planks_load_node($node_type) {
  $nodes = &drupal_static(__FUNCTION__, array());

  if (!isset($nodes[$node_type])) {
    $nodes[$node_type] = array();

    $result = db_select('node', 'n')
      ->fields('n', array('nid', 'title'))
      ->condition('type', $node_type)
      ->orderBy('n.title')
      ->execute();
    while ($row = $result->fetch()) {
      $nodes[$node_type][$row->nid] = $row->title;
    }
  }

  return $nodes[$node_type];
}
function gnome_planks_gnome_planks_add_node($node_type, $box, $context) {
  $url = 'node/add/' . str_replace('_', '-', $node_type);
  return array(
    $url,
    array(
      'query' => array(
        'context' => $context,
        'box' => $box->name
      )
    )
  );
}
function gnome_planks_form_alter(&$form, &$form_state) {
  if (empty($form['#node_edit_form'])) {
    return;
  }

  if (!user_access('administer gnome_planks items')) {
    return;
  }

  if (empty($_GET['context'])) {
    return;
  }
  if (empty($_GET['box'])) {
    return;
  }

  $box = gnome_planks_box_load($_GET['box']);
  if (!$box) {
    return;
  }
  $context = $_GET['context'];

  $form['#gnome_planks'] = array(
    'box' => $box,
    'context' => $context,
  );
}
function gnome_planks_node_submit(&$node, $form, &$form_state) {
  if (!empty($form['#gnome_planks'])) {
    $node->gnome_planks = $form['#gnome_planks'];
  }
}

function gnome_planks_node_update($node) {
  if (!empty($node->gnome_planks)) {
    gnome_planks_entity_add($node->gnome_planks['box'], $node->gnome_planks['context'], 'node', $node->nid);
  }
}
function gnome_planks_node_insert($node) {
  if (!empty($node->gnome_planks)) {
    gnome_planks_entity_add($node->gnome_planks['box'], $node->gnome_planks['context'], 'node', $node->nid);
  }
}

function gnome_planks_entity_add($box, $context, $entity_type, $entity_id) {
  $weights = db_select('gnome_planks_items', 'i')->fields('i', array('weight'))
    ->condition('box', $box->name)->condition('context', $context)
    ->execute()->fetchALL(PDO::FETCH_COLUMN);
  $weight = empty($weights) ? 0 : (max($weights) + 1);

  $record = array(
    'box' => $box->name,
    'context' => $context,
    'weight' => $weight,
    'entity_type' => $entity_type,
    'entity_id' => $entity_id,
  );
  drupal_write_record('gnome_planks_items', $record);
}