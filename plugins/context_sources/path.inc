<?php

/**
 * @file
 * CTools Gnome Planks Context Source Plugin implementation for paths.
 */

function gnome_planks_path_gnome_planks_context_sources() {
  return array(
    'path' => array(
      'label' => 'Path',
      'source class' => 'gnome_planks_context_source_path'
    )
  );
}

class gnome_planks_context_source_path {
  function get_value() {
    return $_GET['q'];
  }
}