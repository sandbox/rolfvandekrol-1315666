<?php

/**
 * @file
 * CTools Gnome Planks Context Handler Plugin implementation for nodes.
 */

function gnome_planks_node_gnome_planks_context_handlers() {
  return array(
    'node' => array(
      'label' => 'Node',
      'handler class' => 'gnome_planks_context_handler_node',
      'sources' => array('path'),
      'default options' => array(),
      'options form' => 'gnome_planks_context_handler_node_options_form',
      'options summary' => 'gnome_planks_context_handler_node_options_summary',
      'options cleanup' => 'gnome_planks_context_handler_node_options_cleanup',
      'context chooser' => 'gnome_planks_context_handler_node_context_chooser',
    ),
    'node_type' => array(
      'label' => 'Node Type',
      'handler class' => 'gnome_planks_context_handler_node_type',
      'sources' => array('path'),
      'default options' => array(),
      'options form' => 'gnome_planks_context_handler_node_options_form',
      'options summary' => 'gnome_planks_context_handler_node_options_summary',
      'options cleanup' => 'gnome_planks_context_handler_node_options_cleanup',
      'context chooser' => 'gnome_planks_context_handler_node_type_context_chooser',
    ),
  );
}

class gnome_planks_context_handler_node {
  var $options;

  function __construct($options) {
    $this->options = $options;
  }
  function get_node($value) {
    if (arg(0, $value) == 'node' && is_numeric(arg(1, $value)) && !arg(2, $value) && ($node = node_load(arg(1, $value)))) {
      return $node;
    }
  }
  function handle_value($value) {
    $node = $this->get_node($value);

    if ($node && (empty($this->options['node_types']) || !empty($this->options['node_types'][$node->type]))) {
      return $node->nid;
    }
  }
}
class gnome_planks_context_handler_node_type extends gnome_planks_context_handler_node {
  var $options;

  function handle_value($value) {
    $node = $this->get_node($value);

    if ($node && (empty($this->options['node_types']) || !empty($this->options['node_types'][$node->type]))) {
      return $node->type;
    }
  }
}

function gnome_planks_context_handler_node_options_form(&$form, $options) {
  $form['node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node types'),
    '#options' => node_type_get_names(),
    '#default_value' => !empty($options['node_types']) ? $options['node_types'] : array()
  );
}
function gnome_planks_context_handler_node_options_summary($options) {
  $types = node_type_get_names();

  return (!empty($options['node_types']) && array_filter($options['node_types'])) ? implode(', ', array_values(array_intersect_key($types, array_filter($options['node_types'])))) : t('All');
}

function gnome_planks_context_handler_node_options_cleanup(&$options) {
  $options['node_types'] = !empty($options['node_types']) ? array_filter($options['node_types']) : array();
}

function gnome_planks_context_handler_node_context_chooser($options) {
  gnome_planks_context_handler_node_options_cleanup($options);

  $query = db_select('node')->fields('node', array('nid', 'title'))->orderBy('created', 'DESC');
  if ($options['node_types']) {
    $query->condition('type', array_keys($options['node_types']), 'IN');
  }

  $nodes = $query->execute()->fetchAllAssoc('nid');
  $options = array();
  foreach ($nodes as $nid => $row) {
    $options[$nid] = $row->title;
  }

  return array(
    '#type' => 'select',
    '#title' => t('Node'),
    '#options' => $options,
    '#emtpy_title' => t('- Select -')
  );
}
function gnome_planks_context_handler_node_type_context_chooser($options) {
  gnome_planks_context_handler_node_options_cleanup($options);
  $types = node_type_get_names();

  $options = array_intersect_key($types, array_filter($options['node_types']));

  return array(
    '#type' => 'select',
    '#title' => t('Node'),
    '#options' => $options,
    '#emtpy_title' => t('- Select -')
  );
}