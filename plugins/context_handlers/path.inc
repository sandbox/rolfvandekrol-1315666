<?php

/**
 * @file
 * CTools Gnome Planks Context Handler Plugin implementation for paths.
 */

function gnome_planks_path_gnome_planks_context_handlers() {
  return array(
    'path' => array(
      'label' => 'Path',
      'handler class' => 'gnome_planks_context_handler_path',
      'sources' => array('path'),
      'default options' => array(),
      'options form' => 'gnome_planks_context_handler_path_options_form',
      'options summary' => 'gnome_planks_context_handler_path_options_summary',
      'options cleanup' => 'gnome_planks_context_handler_path_options_cleanup',
      'context chooser' => 'gnome_planks_context_handler_path_context_chooser',
    ),
  );
}

class gnome_planks_context_handler_path {
  var $options;

  function __construct($options) {
    $this->options = $options;
  }

  function match($subject, $patterns) {
    static $regexps;
    $match = FALSE;
    $positives = $negatives = 0;
    $subject = !is_array($subject) ? array($subject) : $subject;
    foreach ($patterns as $pattern) {
      if (strpos($pattern, '~') === 0) {
        $negate = TRUE;
        $negatives++;
      }
      else {
        $negate = FALSE;
        $positives++;
      }
      $pattern = ltrim($pattern, '~');
      if (!isset($regexps[$pattern])) {
        $regexps[$pattern] = '/^(' . preg_replace(array('/(\r\n?|\n)/', '/\\\\\*/', '/(^|\|)\\\\<front\\\\>($|\|)/'), array('|', '.*', '\1' . preg_quote(variable_get('site_frontpage', 'node'), '/') . '\2'), preg_quote($pattern, '/')) . ')$/';
      }
      foreach ($subject as $value) {
        if (preg_match($regexps[$pattern], $value)) {
          if ($negate) {
            return FALSE;
          }
          $match = TRUE;
        }
      }
    }
    // If there are **only** negative conditions and we've gotten this far none
    // we actually have a match.
    if ($positives === 0 && $negatives) {
      return TRUE;
    }
    return $match;
  }

  function handle_value($value) {
    $paths = array($value);
    $alias = drupal_get_path_alias($value);
    if ($alias != $value) {
      $paths[] = $alias;
    }
    $match = $this->match($paths, array(!empty($this->options['patterns']) ? $this->options['patterns'] : ''));

    if (!$match) {
      return;
    }

    if (!empty($this->options['reduce'])) {
      return $this->options['reduce'];
    }
    else{
      return $value;
    }
  }
}

function gnome_planks_context_handler_path_options_form(&$form, $options) {
  $form['patterns'] = array(
    '#type' => 'textarea',
    '#title' => t('Patterns'),
    '#default_value' => !empty($options['patterns']) ? $options['patterns'] : array()
  );

  $form['reduce'] = array(
    '#type' => 'textfield',
    '#title' => t('Reduce'),
    '#default_value' => !empty($options['reduce']) ? $options['reduce'] : array()
  );
}
function gnome_planks_context_handler_path_options_summary($options) {
  return !empty($options['reduce']) ? t('Set of paths reduced to @reduce', array('@reduce' => $options['reduce'])) : t('Set of paths');
}

function gnome_planks_context_handler_path_options_cleanup(&$options) {
}

function gnome_planks_context_handler_path_context_chooser($options) {
  if (!empty($options['reduce'])) {
    return array(
      '#type' => 'value',
      '#value' => $options['reduce']
    );
  }
  else{
    return array(
      '#type' => 'textfield',
      '#title' => t('Path')
    );
  }
}