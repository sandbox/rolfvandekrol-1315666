<?php

/**
 * @file
 * CTools Export UI Plugin implementation for Gnome Planks.
 */

$plugin = array(
  'schema' => 'gnome_planks_boxes',

  'menu' => array(
    'menu prefix' => 'admin/structure',
    'menu item' => 'gnome_planks',
    'menu title' => 'Gnome Planks',
    'menu description' => 'Administer Gnome Planks Boxes'
  ),

  'export' => array(
    'admin_title' => 'label',
    'admin_description' => 'description',
  ),

  'title singular' => t('box'),
  'title singular proper' => t('Box'),
  'title plural' => t('boxes'),
  'title plural proper' => t('Boxes'),

  'form' => array(
    'settings' => 'gnome_planks_box_form',
    'submit' => 'gnome_planks_box_form_submit',
  )
);

