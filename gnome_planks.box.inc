<?php

/**
 * @file
 * Contains all the functions that relate to the management of boxes and the
 * exportability of them.
 */

/**
 * CTools Export subrecords callback. Loads the data for the context handlers
 * from the database.
 */
function gnome_planks_box_load_subrecords(&$cache) {
  // We will only get the boxes that come from the database here. Due to the
  // way this is constructed, it is possible that some objects already have the
  // subrecords loaded, and others don't, so we need to check which items we
  // should load. We do not have to bother about caching, because CTools already
  // maintains a cache for these object.
  gnome_planks_box_load_subrecords_context_handlers($cache);
  gnome_planks_box_load_subrecords_entity_types($cache);
}
function gnome_planks_box_load_subrecords_context_handlers(&$cache) {
  // find the boxes we need to load and give them an empty container to put
  // the context handlers
  $keys = array();
  foreach ($cache as $key => $box) {
    if (!isset($box->context_handlers)) {
      $keys[$key] = $box->name;
      $box->context_handlers = array();
    }
  }

  if (empty($keys)) {
    return;
  }

  // get the data for the boxes and push it into the objects
  $result = db_select('gnome_planks_box_context_handlers', 'ch')->fields('ch')
    ->condition('box', array_values($keys), 'IN')->orderBy('weight')->execute()->fetchAll();
  $data = array();
  foreach ($result as $item) {
    empty($data[$item->box]) && ($data[$item->box] = array());
    empty($data[$item->box][$item->source]) && ($data[$item->box][$item->source] = array());
    $data[$item->box][$item->source][] = (object) array(
      'handler' => $item->handler,
      'label' => $item->label,
      'options' => unserialize($item->options),
    );
  }

  foreach ($keys as $key => $name) {
    if (!empty($data[$name])) {
      $cache[$key]->context_handlers = $data[$name];
    }
  }
}
function gnome_planks_box_load_subrecords_entity_types(&$cache) {
  // find the boxes we need to load and give them an empty container to put
  // the entity_types
  $keys = array();
  foreach ($cache as $key => $box) {
    if (!isset($box->entity_types)) {
      $keys[$key] = $box->name;
      $box->entity_types = array();
    }
  }

  if (empty($keys)) {
    return;
  }

  // get the data for the boxes and push it into the objects
  $result = db_select('gnome_planks_box_entity_types', 'et')->fields('et')
    ->condition('box', array_values($keys), 'IN')->execute()->fetchAll();
  $data = array();
  foreach ($result as $item) {
    empty($data[$item->box]) && ($data[$item->box] = array());
    empty($data[$item->box][$item->entity_type]) && ($data[$item->box][$item->entity_type] = array());
    $data[$item->box][$item->entity_type][$item->entity_bundle] = $item->view_mode;
  }

  foreach ($keys as $key => $name) {
    if (!empty($data[$name])) {
      $cache[$key]->entity_types = $data[$name];
    }
  }
}

/**
 * CTools Export create callback. Return an empty, unconfigured, box.
 */
function gnome_planks_box_crud_create_callback($set_defaults) {
  $item = ctools_export_new_object('gnome_planks_boxes', $set_defaults);
  $item->context_handlers = array();
  return $item;
}

/**
 * CTools Export save callback. Save the box to the database
 */
function gnome_planks_box_crud_save_callback(&$object) {
  if ($object->export_type & EXPORT_IN_DATABASE) {
    // Existing record.
    $update = array('name');
  }
  else {
    // New record.
    $update = array();
    $object->export_type = EXPORT_IN_DATABASE;
  }

  // write the box
  $return = drupal_write_record('gnome_planks_boxes', $object, $update);

  // remove all the existing handlers
  db_delete('gnome_planks_box_context_handlers')->condition('box', $object->name)->execute();

  // find the available handlers to be able to call the options cleanup
  $context_handlers = gnome_planks_get_context_handlers();

  // save the handlers
  if (array_filter($object->context_handlers)) {
    $query = db_insert('gnome_planks_box_context_handlers')->fields(array('box', 'source', 'weight', 'handler', 'label', 'options'));
    foreach ($object->context_handlers as $source => &$handlers) {
      $weight = 0;
      foreach ($handlers as $delta => &$handler) {
        // call the options cleanup function, if available
        $cleanup_function = ctools_plugin_get_function($context_handlers[$handler->handler], 'options cleanup');
        if ($cleanup_function) {
          $cleanup_function($handler->options);
        }

        $query->values(array(
          'box' => $object->name,
          'source' => $source,
          'weight' => $weight++,
          'handler' => $handler->handler,
          'label' => $handler->label,
          'options' => serialize($handler->options)
        ));
      }
    }
    $query->execute();
  }

  db_delete('gnome_planks_box_entity_types')->condition('box', $object->name)->execute();

  $entity_types = array_filter(array_map('array_filter', $object->entity_types));
  if ($entity_types) {
    $query = db_insert('gnome_planks_box_entity_types')->fields(array('box', 'entity_type', 'entity_bundle', 'view_mode'));
    foreach ($entity_types as $entity_type => $entity_data) {
      foreach ($entity_data as $bundle => $view_mode) {
        $query->values(array(
          'box' => $object->name,
          'entity_type' => $entity_type,
          'entity_bundle' => $bundle,
          'view_mode' => $view_mode
        ));
      }
    }
    $query->execute();
  }

  return $return;
}

/**
 * CTools Export export callback. Creates a string representation of a box.
 */
function gnome_planks_box_crud_export_callback($object, $indent = '') {
  // run the normal logic, but add the data for the context_handlers
  return ctools_export_object('gnome_planks_boxes', $object, $indent, NULL, array(
    'context_handlers' => $object->context_handlers,
    'entity_types' => $object->entity_types,
  ));
}

/**
 * CTools Export delete callback. Removes the object from the database.
 */
function gnome_planks_box_crud_delete_callback($object) {
  $name = is_object($object) ? $object->name : $object;

  db_delete('gnome_planks_box_context_handlers')->condition('box', $name)->execute();
  db_delete('gnome_planks_boxes')->condition('name', $name)->execute();
}

/**
 * CTools Export UI form callback. Creates the form to manage the context
 * handlers.
 */
function gnome_planks_box_form(&$form, &$form_state) {
  $item = $form_state['item'];

  // prepare the handler array in the form state, because that array is going
  // to contain all the data for the handlers
  if (!isset($form_state['handlers'])) {
    $form_state['handlers'] = array();
    foreach ($item->context_handlers as $source => $handlers) {
      $form_state['handlers'][$source] = array();
      foreach ($handlers as $weight => $handler_info) {
        $form_state['handlers'][$source][_gnome_planks_id()] = array(
          'handler' => $handler_info->handler,
          'label' => $handler_info->label,
          'options' => $handler_info->options,
          'weight' => $weight,
        );
      }
    }
  }

  // ajax defaults
  $ajax = array(
    '#ajax' => array(
      'callback' => 'gnome_planks_box_form_ajax_callback',
      'wrapper' => 'gnome-planks-context-handlers-container',
    ),
  );

  // provide a fieldset to put our logic in
  $form['handlers'] = array(
    '#type' => 'fieldset',
    '#title' => t('Handlers'),
    '#tree' => TRUE,
  );

  // get alle the context sources and context handlers
  $context_sources = gnome_planks_get_context_sources();
  $context_handlers = gnome_planks_get_context_handlers();

  // simple array to contain the available sources for the add source select
  // below
  $context_sources_options = array();

  // loop over the available context sources and fill the form if it is used
  foreach ($context_sources as $name => $source_plugin) {
    // add the source to the list
    $context_sources_options[$name] = t($source_plugin['label']);

    // we have no handlers for this source, we do not display the fieldset
    if (isset($form_state['handlers'][$name]) && !empty($form_state['handlers'][$name])) {
      // add the fieldset for this source
      $form['handlers']['source_' . $name] = array(
        '#type' => 'fieldset',
        '#title' => check_plain(t($source_plugin['label'])),
        '#theme' => 'gnome_planks_handlers_admin', // will provide the table
      );

      // walk over the registered handlers
      foreach ($form_state['handlers'][$name] as $id => $handler_info) {
        // create a summary of the choosen options
        $summary_function = ctools_plugin_get_function($context_handlers[$handler_info['handler']], 'options summary');
        if ($summary_function) {
          $summary = $summary_function($handler_info['options']);
        }
        else{
          $summary = '';
        }

        // prepare a table row for this handler
        $form['handlers']['source_' . $name][$id] = array(
          'handler_label' => array('#markup' => $context_handlers[$handler_info['handler']]['label']),
          'options_summary' => array('#markup' => $summary),
          'weight' => array('#type' => 'weight', '#default_value' => $handler_info['weight'], '#delta' => 128),
          'label' => array('#type' => 'textfield', '#default_value' => $handler_info['label'], '#size' => 30),

          // container for the options
          'options' => array(
            '#type' => 'container',
            '#attributes' => array('class' => array('gnome-planks-context-handler-options')),

            // actions for the options
            'actions' => array(
              '#type' => 'container',
              '#attributes' => array('class' => array('container-inline')),
              '#weight' => 100,

              'close' => array(
                '#type' => 'submit',
                '#value' => t('Close'),
                '#submit' => array('gnome_planks_box_form_context_handler_configure_close_submit'),
              ) + $ajax,
              'delete' => array(
                '#type' => 'submit',
                '#value' => t('Delete'),
                '#name' => 'handlers_' . $name . '_' . $id . '_delete_button',
                '#submit' => array('gnome_planks_box_form_context_handler_delete_submit'),
                '#handler_id' => $id,
                '#handler_source' => $name,
              ) + $ajax,
            ),

          ),
          'configure' => array(
            '#type' => 'image_button',
            '#src' => 'misc/configure.png',
            '#submit' => array('gnome_planks_box_form_context_handler_configure_submit'),
            '#handler_id' => $id,
          ) + $ajax,
        );

        // add the handler specific form elements
        if ($form_function = ctools_plugin_get_function($context_handlers[$handler_info['handler']], 'options form')) {
          $form_function($form['handlers']['source_' . $name][$id]['options'], $handler_info['options']);
        }

        // show and hide the options container, the configure button and the
        // options summary, based on whether this should be the open handler
        if (isset($form_state['handler_options_open']) && $form_state['handler_options_open'] == $id) {
          $form['handlers']['source_' . $name][$id]['options']['#access'] = TRUE;
          $form['handlers']['source_' . $name][$id]['configure']['#access'] = $form['handlers']['source_' . $name][$id]['options_summary']['#access'] = FALSE;
        }
        else{
          $form['handlers']['source_' . $name][$id]['options']['#access'] = FALSE;
          $form['handlers']['source_' . $name][$id]['configure']['#access'] = $form['handlers']['source_' . $name][$id]['options_summary']['#access'] = TRUE;
        }
      }
    }
  }

  // create a list of available context handlers for every context sources
  $context_handlers_options = array_combine(array_keys($context_sources), array_fill(0, count($context_sources), array()));
  foreach ($context_handlers as $name => $handler_plugin) {
    // handlers can be for a specific set of sources or for all sources
    if (isset($handler_plugin['sources'])) {
      foreach ($handler_plugin['sources'] as $source) {
        if (isset($context_handlers_options[$source])) {
          $context_handlers_options[$source][$name] = t($handler_plugin['label']);
        }
      }
    }
    else{
      foreach (array_keys($context_sources) as $source) {
        $context_handlers_options[$source][$name] = t($handler_plugin['label']);
      }
    }
  }
  $context_handlers_options = array_filter($context_handlers_options);

  // create the UI to add handlers
  $form['handlers']['add'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('container-inline')),
  );

  $form['handlers']['add']['source'] = array(
    '#type' => 'select',
    '#title' => t('Source'),
    '#options' => $context_sources_options,
    '#empty_option' => t('- Select -'),
  ) + $ajax;
  if (isset($form_state['values']['handlers']['add']['source']) && $form_state['values']['handlers']['add']['source']) {
    $form['handlers']['add']['handler'] = array(
      '#type' => 'select',
      '#title' => t('Handler'),
      '#options' => $context_handlers_options[$form_state['values']['handlers']['add']['source']],
      '#empty_option' => t('- Select -'),
    );
  }

  $form['handlers']['add']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
    '#submit' => array('gnome_planks_box_form_context_handlers_add_submit'),
  ) + $ajax;

  // wrap the whole handlers configuration in a container to be able to do AJAX
  $form['handlers_container'] = array(
    '#type' => 'container',
    '#attributes' => array('id' => 'gnome-planks-context-handlers-container'),
    'handlers' => $form['handlers']
  );
  unset($form['handlers']);

  $entity_info = gnome_planks_get_entity_info();

  $form['entity_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Entity types'),
    '#tree' => TRUE,
  );

  foreach ($entity_info as $entity_type => $entity_data) {
    $form['entity_types'][$entity_type] = array(
      '#type' => 'fieldset',
      '#title' => check_plain($entity_data['label']),
    );

    $options = array();
    foreach ($entity_data['view modes'] as $view_mode => $mode_data) {
      $options[$view_mode] = $mode_data['label'];
    }

    foreach ($entity_data['bundles'] as $bundle_key => $bundle_data) {
      $form['entity_types'][$entity_type][$bundle_key] = array(
        '#type' => 'select',
        '#title' => check_plain($bundle_data['label']),
        '#empty_option' => t('-Select-'),
        '#options' => $options,
        '#default_value' => !empty($item->entity_types[$entity_type][$bundle_key]) ? $item->entity_types[$entity_type][$bundle_key] : ''
      );
    }
  }
}

/**
 * Form submit callback. This gets fired when the user clicks the 'add' button.
 * It adds a new handler array to the form_state.
 */
function gnome_planks_box_form_context_handlers_add_submit($form, &$form_state) {
  $handlers = gnome_planks_get_context_handlers();

  gnome_planks_box_form_context_handlers_update($form, $form_state);

  if (isset($form_state['values']['handlers']['add']['source']) && $form_state['values']['handlers']['add']['source'] &&
      isset($form_state['values']['handlers']['add']['handler']) && $form_state['values']['handlers']['add']['handler']) {
    $weight = 0;
    foreach ($form_state['handlers'][$form_state['values']['handlers']['add']['source']] as $id => $handler_info) {
      $weight = max($weight, intval($handler_info['weight']));
    }
    $form_state['handlers'][$form_state['values']['handlers']['add']['source']][_gnome_planks_id()] = array(
      'handler' => $form_state['values']['handlers']['add']['handler'],
      'options' => isset($handlers[$form_state['values']['handlers']['add']['handler']]['default options']) ? $handlers[$form_state['values']['handlers']['add']['handler']]['default options'] : array(),
      'label' => '',
      'weight' => $weight + 1,
    );
  }

  $form_state['rebuild'] = TRUE;
}

/**
 * Form submit callback. This gets fired when the user clicks the 'configure'
 * button. It sets the handler for which is was set to be the open handler.
 */
function gnome_planks_box_form_context_handler_configure_submit($form, &$form_state) {
  gnome_planks_box_form_context_handlers_update($form, $form_state);

  $form_state['handler_options_open'] = $form_state['triggering_element']['#handler_id'];

  $form_state['rebuild'] = TRUE;
}
/**
 * Form submit callback. This gets fired when the user clicks the 'close'
 * button. It clears the open handler
 */
function gnome_planks_box_form_context_handler_configure_close_submit($form, &$form_state) {
  gnome_planks_box_form_context_handlers_update($form, $form_state);

  $form_state['handler_options_open'] = NULL;

  $form_state['rebuild'] = TRUE;
}

/**
 * Form submit callback. This gets fired when the user clicks the 'delete'
 * button. It removes the handler for which is was set from the form_state.
 */
function gnome_planks_box_form_context_handler_delete_submit($form, &$form_state) {
  if (isset($form_state['handlers'][$form_state['triggering_element']['#handler_source']][$form_state['triggering_element']['#handler_id']])) {
    unset($form_state['handlers'][$form_state['triggering_element']['#handler_source']][$form_state['triggering_element']['#handler_id']]);
  }

  gnome_planks_box_form_context_handlers_update($form, $form_state);

  $form_state['handler_options_open'] = NULL;

  $form_state['rebuild'] = TRUE;
}

/**
 * CTools Export UI form submit callback. Translates the handler array in the
 * form_state to the context_handlers array on the box object.
 */
function gnome_planks_box_form_submit($form, &$form_state) {
  $handlers = gnome_planks_get_context_handlers();
  $sources = gnome_planks_get_context_sources();

  gnome_planks_box_form_context_handlers_update($form, $form_state);

  $result = array();

  foreach ($sources as $source => $source_plugin) {
    if (!empty($form_state['handlers'][$source])) {
      $result[$source] = array();

      foreach ($form_state['handlers'][$source] as $id => $handler_info) {
        if (!empty($handlers[$handler_info['handler']])) {
          $result[$source][] = (object) array(
            'handler' => $handler_info['handler'],
            'options' => $handler_info['options'],
            'label' => $handler_info['label'],
          );
        }
      }
    }
  }

  $result = array_filter($result);

  $form_state['item']->context_handlers = $result;
  $form_state['item']->entity_types = !empty($form_state['values']['entity_types']) ? array_filter(array_map('array_filter', $form_state['values']['entity_types'])) : array();
}

/**
 * Theme callback. Creates the table of handlers in the admin UI.
 */
function theme_gnome_planks_handlers_admin($variables) {
  $elements = $variables['elements'];

  $rows = array();

  foreach (element_children($elements) as $key) {
    $elements[$key]['weight']['#attributes']['class'][] = 'gnome-planks-handlers-weight';
    hide($elements[$key]['weight']);

    if ($elements[$key]['configure']['#access']) {
      hide($elements[$key]['configure']);
      hide($elements[$key]['options_summary']);
      hide($elements[$key]['label']);

      $rows[] = array(
        'data' => array(
          render($elements[$key]),
          render($elements[$key]['label']),
          render($elements[$key]['weight']),
          render($elements[$key]['options_summary']),
          render($elements[$key]['configure']),
        ),
        'class' => array('draggable'),
      );
    }
    else{
      hide($elements[$key]['options']);
      hide($elements[$key]['label']);

      $rows[] = array(
        'data' => array(
          render($elements[$key]),
          render($elements[$key]['label']),
          render($elements[$key]['weight']),
          array('data' => render($elements[$key]['options']), 'colspan' => 2),
        ),
        'class' => array('draggable'),
      );
    }
  }

  $header = array(t('Handler'), t('Label'), t('Weight'), array('data' => t('Configuration'), 'colspan' => 2));

  drupal_add_tabledrag($elements['#id'] . '-table', 'order', 'sibling', 'gnome-planks-handlers-weight');
  return theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => $elements['#id'] . '-table')));
}

/**
 * AJAX Callback. Will be called for every AJAX action in the box edit form
 */
function gnome_planks_box_form_ajax_callback($form, $form_state) {
  return $form['handlers_container'];
}

/**
 * Update the handlers array in the form_state, based on the values array in the
 * form_state
 */
function gnome_planks_box_form_context_handlers_update($form, &$form_state) {
  $handlers = gnome_planks_get_context_handlers();
  $sources = gnome_planks_get_context_sources();

  foreach ($form_state['handlers'] as $source => &$handlers) {
    foreach ($handlers as $id => &$handler_info) {
      if (isset($form_state['values']['handlers']['source_' . $source][$id]['options'])) {
        $handler_info['options'] = $form_state['values']['handlers']['source_' . $source][$id]['options'];
        unset($handler_info['options']['actions']);
      }
      if (isset($form_state['values']['handlers']['source_' . $source][$id]['weight'])) {
        $handler_info['weight'] = $form_state['values']['handlers']['source_' . $source][$id]['weight'];
      }
      if (isset($form_state['values']['handlers']['source_' . $source][$id]['label'])) {
        $handler_info['label'] = $form_state['values']['handlers']['source_' . $source][$id]['label'];
      }
    }

    uasort($handlers, 'gnome_planks_box_form_handler_info_cmp');
  }
}
/**
 * Compare callback.
 */
function gnome_planks_box_form_handler_info_cmp($a, $b) {
  $a_weight = intval($a['weight']);
  $b_weight = intval($b['weight']);

  return ($a_weight > $b_weight) ? 1 : (($a_weight < $b_weight) ? -1 : 0);
}