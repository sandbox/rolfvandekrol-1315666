<?php

/**
 * @file
 * Management interface for the items in the boxes.
 */

function gnome_planks_admin_page() {
  $rows = array();

  ctools_include('export');
  $result = ctools_export_load_object('gnome_planks_boxes', 'all');

  foreach ($result as $box) {
    $rows[] = array(
      $box->label,
      l(t('Manage contexts'), 'admin/content/gnome_planks/' . $box->name),
    );
  }

  return array(
    '#theme' => 'table',
    '#header' => array(t('Box'), t('Operations')),
    '#rows' => $rows,
  );
}

function gnome_planks_admin_box_page($box) {
  return drupal_get_form('gnome_planks_box_context_select_form', $box);
}

function gnome_planks_box_context_select_form($form, &$form_state, $box) {
  $context_sources = gnome_planks_get_context_sources();
  $context_handlers = gnome_planks_get_context_handlers();

  $form['#box'] = $box;

  // ajax defaults
  $ajax = array(
    '#ajax' => array(
      'callback' => 'gnome_planks_box_context_select_form_ajax_callback',
      'wrapper' => 'gnome-planks-context-select-' . $box->name,
    ),
  );

  $form['context'] = array(
    '#type' => 'container',
    '#theme' => 'gnome_planks_box_context_select_form_table',
    '#attributes' => array('id' => $ajax['#ajax']['wrapper']),
    '#tree' => TRUE,
  );

  foreach ($context_sources as $name => $source_plugin) {
    if (!empty($box->context_handlers[$name])) {
      $form['context']['source_' . $name] = array(
        '#source' => $source_plugin,
      );

      $handler_options = array();
      foreach ($box->context_handlers[$name] as $weight => $handler_info) {
        $handler_options[$weight] = t('@label (@handler)', array('@label' => $handler_info->label, '@handler' => $context_handlers[$handler_info->handler]['label']));
      }

      $form['context']['source_' . $name]['handler'] = array(
        '#type' => 'select',
        '#title' => t('Handler'),
        '#options' => $handler_options,

        '#empty_value' => 'none',
        '#empty_option' => t('- Select -'),
      )+$ajax;

      if (isset($form_state['values']['context']['source_' . $name]['handler']) && $form_state['values']['context']['source_' . $name]['handler'] != 'none') {
        $handler_info = $box->context_handlers[$name][$form_state['values']['context']['source_' . $name]['handler']];
        $form['context']['source_' . $name]['chooser'] = $context_handlers[$handler_info->handler]['context chooser']($handler_info->options);
      }
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Find context')
  );

  return $form;
}

function gnome_planks_box_context_select_form_submit($form, &$form_state) {
  $context_sources = gnome_planks_get_context_sources();
  $box = $form['#box'];

  $context = array();
  foreach ($context_sources as $name => $source_plugin) {
    if (!empty($box->context_handlers[$name])) {
      $context[$name] = array(
        $box->context_handlers[$name][$form_state['values']['context']['source_' . $name]['handler']]->handler,
        $form_state['values']['context']['source_' . $name]['chooser']
      );
    }
  }

  $hash = gnome_planks_ensure_context($context);

  $form_state['redirect'] = 'admin/content/gnome_planks/' . $box->name . '/' . $hash;
}

function gnome_planks_admin_box_items_page($box, $context_hash) {
  return array(
    'form' => drupal_get_form('gnome_planks_admin_box_items_form', $box, $context_hash),
  );
}

function gnome_planks_admin_box_items_form($form, &$form_state, $box, $context_hash) {
  $items = gnome_planks_get_items($box, $context_hash);
  $entity_info = entity_get_info();

  $entity_data = array();
  $entity_ids = array();
  foreach ($items as $item_id => $item) {
    $entity_ids[$item->entity_type][] = $item->entity_id;
  }

  foreach ($entity_ids as $entity_type => $entity_ids) {
    $info = $entity_info[$entity_type];

    $query = db_select($info['base table']);
    $fields = array($info['entity keys']['id']);
    if (!empty($info['entity keys']['bundle'])) {
      $fields[] = $info['entity keys']['bundle'];
    }
    if (!empty($info['entity keys']['label'])) {
      $fields[] = $info['entity keys']['label'];
    }
    $query->fields($info['base table'], $fields);
    $query->condition($info['entity keys']['id'], $entity_ids, 'IN');

    $result = $query->execute()->fetchAllAssoc($info['entity keys']['id'], PDO::FETCH_ASSOC);

    foreach ($result as $entity_id => $row) {
      $data = array();
      if (!empty($info['entity keys']['bundle'])) {
        $data['bundle'] = $row[$info['entity keys']['bundle']];
        if (!empty($info['bundles'][$data['bundle']]['label'])) {
          $data['bundle'] = $info['bundles'][$data['bundle']]['label'];
        }
      }
      if (!empty($info['entity keys']['label'])) {
        $data['label'] = $row[$info['entity keys']['label']];
      }

      $entity_data[$entity_type][$entity_id] = $data;
    }
  }

  $form['items'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
    '#theme' => 'gnome_planks_admin_box_items_form_table'
  );

  foreach ($items as $item_id => $item) {
    $form['items'][$item_id] = array();

    $data = !empty($entity_data[$item->entity_type][$item->entity_id]) ? $entity_data[$item->entity_type][$item->entity_id] : array();
    $form['items'][$item_id]['#entity_type'] = $entity_info[$item->entity_type]['label'];

    if (!empty($data['bundle'])) {
      $form['items'][$item_id]['#entity_bundle'] = $data['bundle'];
    }
    if (!empty($data['label'])) {
      $form['items'][$item_id]['#entity_label'] = $data['label'];
    }
    $form['items'][$item_id]['#entity_id'] = $item->entity_id;

    $form['items'][$item_id]['weight'] = array(
      '#type' => 'weight',
      '#delta' => 128,
      '#default_value' => $item->weight
    );

    $form['items'][$item_id]['delete'] = array(
      '#type' => 'link',
      '#href' => 'admin/content/gnome_planks/' . $box->name . '/' . $context_hash . '/' . $item_id . '/delete',
      '#title' => t('Delete')
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

function gnome_planks_admin_box_items_form_submit($form, &$form_state) {
  if (!empty($form_state['values']['items'])) {
    foreach ($form_state['values']['items'] as $item_id => $data) {
      db_update('gnome_planks_items')->fields(array('weight' => $data['weight']))->condition('item_id', $item_id)->execute();
    }
  }
}

function theme_gnome_planks_admin_box_items_form_table($variables) {
  $elements = $variables['elements'];

  $rows = array();

  foreach (element_children($elements) as $item_id) {
    $row = array(
      check_plain($elements[$item_id]['#entity_type']),
    );

    $label = $elements[$item_id]['#entity_id'];
    if (!empty($elements[$item_id]['#entity_label'])) {
      $label = check_plain($elements[$item_id]['#entity_label']);
    }
    if (!empty($elements[$item_id]['#entity_bundle'])) {
      $row[] = check_plain($elements[$item_id]['#entity_bundle']);
      $row[] = $label;
    }
    else{
      $row[] = array('data' => $label, 'colspan' => 2);
    }

    $elements[$item_id]['weight']['#attributes']['class'][] = 'gnome-planks-items-weight';
    $row[] = render($elements[$item_id]['weight']);
    $row[] = render($elements[$item_id]['delete']);

    $rows[] = array(
      'data' => $row,
      'class' => array('draggable'),
    );
  }

  drupal_add_tabledrag($elements['#id'] . '-table', 'order', 'sibling', 'gnome-planks-items-weight');
  return theme('table', array(
    'rows' => $rows,
    'header' => array(t('Entity type'), array('data' => t('Entity'), 'colspan' => 2), t('Weight'), t('Operations')),
    'attributes' => array('id' => $elements['#id'] . '-table'),
  ));
}

function gnome_planks_box_context_select_form_ajax_callback($form, $form_state) {
  return $form['context'];
}

function theme_gnome_planks_box_context_select_form_table($variables) {
  $elements = $variables['elements'];

  $rows = array();
  foreach (element_children($elements) as $key) {
    hide($elements[$key]['handler']);
    unset($elements[$key]['handler']['#title']);

    if (!empty($elements[$key]['chooser'])) {
      hide($elements[$key]['chooser']);
    }

    $rows[] = array(
      check_plain($elements[$key]['#source']['label']) . render($elements[$key]),
      render($elements[$key]['handler']),
      !empty($elements[$key]['chooser']) ? render($elements[$key]['chooser']) : ''
    );
  }

  return theme('table', array(
    'header' => array(t('Source'), t('Handler'), t('Context')),
    'rows' => $rows,
  ));
}

function gnome_planks_admin_box_item_add_form($form, &$form_state, $box, $context_hash) {
  $form['#box'] = $box;
  $form['#context'] = $context_hash;

  $entity_info = gnome_planks_get_entity_info();

  $entity_type_options = array();
  foreach ($entity_info as $entity_type => $entity_data) {
    if (!empty($box->entity_types[$entity_type])) {
      $entity_type_options[$entity_type] = $entity_data['label'];
    }
  }

  $ajax = array(
    '#ajax' => array(
      'callback' => 'gnome_planks_box_item_add_form_ajax_callback',
      'wrapper' => 'gnome-planks-item-add-' . $box->name,
    ),
  );

  $form['item'] = array(
    '#type' => 'container',
    '#attributes' => array('id' => $ajax['#ajax']['wrapper']),
  );

  $form['item']['entity_type'] = array(
    '#type' => 'select',
    '#title' => t('Entity type'),
    '#options' => $entity_type_options,

    '#empty_value' => '',
    '#empty_option' => t('- Select -'),
    '#required' => TRUE,
  )+$ajax;

  if (!empty($form_state['values']['entity_type'])) {
    $entity_bundle_options = array();
    foreach ($entity_info[$form_state['values']['entity_type']]['bundles'] as $bundle => $bundle_data) {
      if (!empty($box->entity_types[$entity_type][$bundle])) {
        $entity_bundle_options[$bundle] = $bundle_data['label'];
      }
    }

    $form['item']['entity_bundle'] = array(
      '#type' => 'select',
      '#title' => t('Entity bundle'),
      '#options' => $entity_bundle_options,

      '#empty_value' => '',
      '#empty_option' => t('- Select -'),
      '#required' => TRUE,
    )+$ajax;

    if (!empty($form_state['values']['entity_bundle'])) {
      $function = $entity_info[$form_state['values']['entity_type']]['gnome_planks_module'] . '_gnome_planks_load_' . $form_state['values']['entity_type'];
      $entity_id_options = $function($form_state['values']['entity_bundle']);

      $form['item']['entity'] = array(
        '#title' => t('Entity'),
        '#theme_wrappers' => array('form_element')
      );
      $form['item']['entity']['sub'] = array(
        '#type' => 'container',
        '#id' => 'gnome-planks-item-add-' . $box->name . '-entity',
        '#attributes' => array('class' => array('container-inline'))
      );

      $form['item']['entity']['sub']['entity_id'] = array(
        '#type' => 'select',
        '#options' => $entity_id_options,

        '#empty_value' => '',
        '#empty_option' => t('- Select -'),
      );

      $form['item']['entity']['sub']['add'] = array(
        '#type' => 'submit',
        '#value' => t('Create new'),
        '#submit' => array(
          'gnome_planks_admin_box_item_add_form_add_submit'
        ),
        '#prefix' => ' ' . t('or') . ' ',
      );
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add')
  );

  return $form;
}

function gnome_planks_admin_box_item_add_form_validate($form, &$form_state) {
  if (!empty($form_state['triggering_element']['#parents'][0]) && $form_state['triggering_element']['#parents'][0] == 'submit') {
    if (empty($form_state['values']['entity_id'])) {
      form_error($form['item']['entity']['entity_id'], t('!name field is required.', array('!name' => $form['item']['entity']['entity_id']['#title'])));
    }
  }
}

function gnome_planks_admin_box_item_add_form_submit($form, &$form_state) {
  $box = $form['#box'];
  $context = $form['#context'];

  $entity_type = $form_state['values']['entity_type'];
  $entity_id = $form_state['values']['entity_id'];

  gnome_planks_entity_add($box, $context, $entity_type, $entity_id);

  $form_state['redirect'] = 'admin/content/gnome_planks/' . $box->name . '/' . $context;
}

function gnome_planks_admin_box_item_add_form_add_submit($form, &$form_state) {
  $entity_info = gnome_planks_get_entity_info();

  $box = $form['#box'];
  $context = $form['#context'];

  $entity_type = $form_state['values']['entity_type'];
  $entity_bundle = $form_state['values']['entity_bundle'];

  $destination = 'admin/content/gnome_planks/' . $box->name . '/' . $context;
  if (!empty($_GET['destination'])) {
    $destination = $_GET['destination'];
  }

  $function = $entity_info[$entity_type]['gnome_planks_module'] . '_gnome_planks_add_' . $entity_type;
  $data = $function($entity_bundle, $box, $context);
  $data[1]['query']['destination'] = $destination;

  $form_state['redirect'] = $data;
}

function gnome_planks_box_item_add_form_ajax_callback($form) {
  return $form['item'];
}

function gnome_planks_admin_box_item_delete_form($form, &$form_state, $box, $context, $item_id) {
  $form['#box'] = $box;
  $form['#context'] = $context;
  $form['#item_id'] = $item_id;

  return confirm_form($form,
    t('Are you sure you want to delete this item?'),
    'admin/content/gnome_planks/' . $box->name . '/' . $context
  );
}
function gnome_planks_admin_box_item_delete_form_submit($form, &$form_state) {
  db_delete('gnome_planks_items')->condition('item_id', $form['#item_id'])->execute();

  $form_state['redirect'] = 'admin/content/gnome_planks/' . $form['#box']->name . '/' . $form['#context'];
}